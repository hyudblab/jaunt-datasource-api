--@block
select td.name, tp.id, tp.servertime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' as servertime,
  tp.devicetime,
  tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' as seoultime
    from tc_devices td, tc_positions tp
    where td.id = tp.deviceid
    and td.name = '정근성_5X'
    and tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' between '2020-06-03' and '2020-06-04'
    order by tp.servertime desc
-- 18423

--@block
SELECT td.name, tp.id, tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' as devicetime
    from tc_devices td, tc_positions tp
    where td.id = tp.deviceid
    and td.name = '정근성_G4'
    order by tp.devicetime desc limit 100

--@block
select td.name, count(tp.id)
    from tc_devices td, tc_positions tp
    where td.id = tp.deviceid
    and tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' between '2020-06-04 07:20' and '2020-06-04 19:20'
    group by td.name    
    -- and tp.id >= 18423

--@block
select td.name, count(tp.id)
    from tc_devices td, tc_positions tp
    where td.id = tp.deviceid
    and tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' between '2020-06-07 07:00' and '2020-06-07 08:00'
    group by td.name    

--@block
select td.name, tp.id, tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' as dtime,
tp.servertime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' as stime,
 tp.attributes
    from tc_devices td, tc_positions tp
    where td.id = tp.deviceid
    and tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' between '2020-06-06 19:00' and '2020-06-07 08:00'    
    and td.uniqueid != '4458'
    order by td.id, dtime
    

--@block
show timezone

--@block
select * from tc_devices