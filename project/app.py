from server_app import create_app, db

if __name__ == "__main__":
    app = create_app()
    port = 5000
    with app.app_context():
        result = db.engine.execute("show timezone")
        app.config["tz"] = [row[0] for row in result][0]
        app.config["mongo_tz"] = "Etc/UTC"  # Mongo Default
        app.config["port"] = port
    app.run(debug=True, host="0.0.0.0", port=port)
