from flask import Blueprint, request, current_app
from .mongo import answer_by_pos_id, answered_by_dids
from .traccar import UserDevice, query_positions, traccar_json_default
from .pre_survey import query_survey_by_did
from sqlalchemy.orm.exc import NoResultFound
from datetime import datetime
import json
import traceback
from pprint import pprint

api = Blueprint("api", __name__)


@api.route("/")
def index():
    return "hello"


@api.route("/user/<deviceid>")
@api.route("/device/<deviceid>")
def userdevice(deviceid):
    try:
        udevice = UserDevice(deviceid)
        result = {"title": f"tc_device-{deviceid}", "data": udevice.device}
        return json.dumps({"status": "OK", "result": result}, default=traccar_json_default)
    except NoResultFound:
        print("No device")
    return json.dumps({"status": "error", "message": "No such device: " + deviceid})


@api.route("/users")
@api.route("/devices")
def all_userdevices():
    query = UserDevice.query_all_devices()
    col_names = [x["name"] for x in query.column_descriptions]
    result = {"title": "all_tc_devices", "columns": col_names, "data": query.all()}
    return json.dumps({"status": "OK", "result": result}, default=traccar_json_default)


@api.route("/user/<deviceid>/surveys")
@api.route("/device/<deviceid>/surveys")
def user_surveys(deviceid):
    try:
        target_tz = request.args.get("tz") or "Asia/Seoul"
        answered_pos_ids = answered_by_dids([deviceid])
        print(answered_pos_ids)
        cols = []
        answers = []
        for pos_id in answered_pos_ids:
            cols, rows = answer_by_pos_id(pos_id, target_tz=target_tz)
            answers = answers + rows
        print(cols)
        pprint(answers)
        result = {"title": "surveys", "columns": cols, "data": answers}
        return json.dumps({"status": "OK", "result": result}, default=traccar_json_default)
    except Exception:
        return json.dumps({"status": "error", "message": "No such device: " + deviceid})


@api.route("/user/<deviceid>/pre-surveys")
@api.route("/device/<deviceid>/pre-surveys")
def user_pre_surveys(deviceid):
    print("1")
    try:

        result = {"title": "surveys", "data": query_survey_by_did(deviceid)}
        return json.dumps({"status": "OK", "result": result}, default=traccar_json_default)
    except Exception:
        return json.dumps({"status": "error", "message": "No such device: " + deviceid})


@api.route("/survey")
def survey():
    try:
        pos_id = int(request.args.get("pos_id"))
        target_tz = request.args.get("tz") or "Asia/Seoul"
        print(pos_id)
        cols, data = answer_by_pos_id(pos_id, target_tz)
        print(cols)
        print(data)
        result = {"title": "answers_on_" + request.args.get("pos_id"), "columns": cols, "data": data}
        return json.dumps({"status": "OK", "result": result}, default=traccar_json_default)
    except Exception:
        traceback.print_exc()
        return json.dumps({"status": "error", "message": "No such answer on: " + request.args.get("pos_id")})


@api.route("/positions")
def positions():
    dids = request.args.getlist("did")
    startdate = request.args.get("startdate") or datetime(2019, 10, 1).strftime("%Y-%m-%d")
    enddate = request.args.get("enddate") or datetime.now().strftime("%Y-%m-%d")
    target_tz = request.args.get("tz") or "Asia/Seoul"
    try:
        query = query_positions(dids, startdate, enddate, current_app.config["mongo_tz"], target_tz)

        col_names = [x["name"] for x in query.column_descriptions]  # + ["stop_survey"]

        title = "positions"
        if len(dids) == 1:
            title += f"_{dids[0]}"
        elif len(dids) > 1:
            title += f"_{dids[0]}_more"

        answered_pos = answered_by_dids(dids, target_tz=target_tz)
        print(answered_pos)
        data = query.all()
        result = {
            "title": title,
            "columns": col_names,
            "data": data,
            "additional": {"description": "Answered positions", "name": "pos_id", "data": answered_pos},
        }
        return json.dumps({"status": "OK", "result": result}, default=traccar_json_default)
    except Exception:
        traceback.print_exc()
        return json.dumps({"status": "error", "message": "No such devices: "})
