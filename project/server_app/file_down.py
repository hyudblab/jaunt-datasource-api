from io import StringIO
import pandas as pd
from flask import Blueprint, Response, current_app, request
import requests
from datetime import datetime
import json
import urllib.parse

file_down = Blueprint("file_down", __name__)  # Flask App 만들기


@file_down.route("/", defaults={"path": ""})
@file_down.route("/<path:path>")
def file_transfer(path):
    port = current_app.config["port"]
    url = f"http://localhost:{port}/api/{path}"
    url += "?"
    for k, v in request.args.items(multi=True):
        url += urllib.parse.urlencode({k: v}) + "&"
    try:
        data = requests.get(url).json()
        if data["status"] != "OK":
            print("Not OK")
    except Exception:
        print("html failed")
    # response = query_to_csv("")
    return dict_to_csv(data["result"])


# open csv with excel https://www.lesstif.com/life/ms-excel-utf-8-csv-54952504.html
def dict_to_csv(data):
    if data.get("columns") is not None:
        df = pd.DataFrame(data["data"], columns=data.get("columns"))
        if data.get("additional") is not None:
            df["surveyed"] = False
            print("add cols")
            target_col = data["additional"]["name"]
            target = data["additional"]["data"]
            print(target_col)
            print(target)
            df.loc[df["pos_id"].isin(target), "surveyed"] = True
        print(df.head())
        output = StringIO()
        df.to_csv(output, index=False)
        response = Response(output.getvalue(), mimetype="text/csv", content_type="application/octet-stream",)
        extension = "csv"
    else:
        response = Response(json.dumps(data), mimetype="text/plain", content_type="application/octet-stream",)
        extension = "txt"
    title = data.get("title") if data.get("title") is not None else "csvfile" + datetime.now().strftime("%Y-%m-%d")
    response.headers["Content-Disposition"] = f"attachment; filename={title}.{extension}"  # 다운받았을때의 파일 이름 지정해주기
    return response
