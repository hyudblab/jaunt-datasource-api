import json

# db_props in 'db.properties' (json) file
# {
#     "db_props": {
#         "drivername": "postgresql",
#         "host": "",
#         "port": "5432",
#         "username": "postgres",
#         "password": "",
#         "database": "",  # for postgres
#         "authdb": "", # for mongo
#     }
# }


def open_db_props_as_url(prop_name, db_type, filename="db.properties"):
    props = get_secret(prop_name, filename)
    url = ""
    if db_type == "pg":
        url = "{drivername}://{username}:{password}@{host}:{port}/{database}".format(**props)
    elif db_type == "mongo":
        url = "{drivername}://{username}:{password}@{host}:{port}/{database}?authSource={authdb}".format(**props)
    else:
        print("[Properties Error]: Bad db type or Wrong db.properties attribute.")
        raise ValueError
    return url


def get_secret(setting, filename):
    with open(filename) as secrets_file:
        secrets = json.load(secrets_file)
    """Get secret setting or fail with ImproperlyConfigured"""
    try:
        return secrets[setting]
    except KeyError:
        print("Set the {} setting".format(setting))
        raise
