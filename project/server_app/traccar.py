from .models import TcDevice, TcPosition
from datetime import datetime, date
from pytz import timezone


ignore_list = ["test", "#%%", "@%%", "01", "02"]

entities = [TcDevice.id, TcDevice.lastupdate]


def traccar_json_default(value):
    # print(isinstance(value, tuple))
    # print(type(value))
    # print(value)
    if isinstance(value, date):
        return value.strftime("%Y-%m-%dT%H:%M:%S")
    raise TypeError("not JSON serializable")


class UserDevice:
    @classmethod
    def query_all_devices(cls, testers=False):
        if testers:
            return TcDevice.query.all()

        regex = "|".join(ignore_list)
        # 'similar to' works only Postgres!
        devices = TcDevice.query.with_entities(*entities).filter(TcDevice.name.op("not similar to")(regex))
        return devices

    @classmethod
    def query_device_by_uniqueid(cls, uid, testers=False):
        return UserDevice.query_all_devices(testers).filter(TcDevice.uniqueid == uid)

    @classmethod
    def query_device_by_deviceid(cls, id, testers=False):
        return UserDevice.query_all_devices(testers).filter(TcDevice.id == id)

    @classmethod
    def uid_from_did(cls, did):
        return TcDevice.query.filter(TcDevice.id == did).one().uniqueid

    def __init__(self, deviceid, testers=False):
        self.deviceid = deviceid
        self.testers = testers
        print(type(UserDevice.query_device_by_deviceid(deviceid).one()))
        self.device = UserDevice.query_device_by_deviceid(deviceid).one()._asdict()


class Position:
    def __init__(
        self, uid, startdate=None, enddate=None, tz=timezone("Etc/UTC"), testers=False,
    ):
        self.uid = uid
        self.startdate = startdate if startdate is not None else datetime(2019, 10, 10, tzinfo=timezone)
        self.enddate = enddate if enddate is not None else datetime.utcnow().astimezone(tz)
        self.tz = tz
        print(uid, startdate, enddate, tz)


def query_positions(dids, startdate, enddate, db_tz, target_tz, testers=False):
    pos_id = TcPosition.id.label("pos_id")
    pos_columns = [pos_id, TcPosition.devicetime, TcPosition.latitude, TcPosition.longitude, TcPosition.altitude]
    pos_entities = entities + pos_columns

    q = (
        TcPosition.query.with_entities(*pos_entities)
        .filter(TcDevice.id.in_(dids))
        .filter(TcDevice.id == TcPosition.deviceid)
        .filter(
            TcPosition.devicetime.op("at time zone")(db_tz).op("at time zone")(target_tz).between(startdate, enddate)
        )
    )
    return q
