from flask import Blueprint, render_template
from sqlalchemy.orm.exc import NoResultFound


main = Blueprint("main", __name__)


@main.route("/")
def index():
    try:
        pass
    except NoResultFound:
        print("device not found")
    return render_template("index.html")


@main.route("/positions")
def positions():
    return render_template("positions.html")


# @main.route("/d")
