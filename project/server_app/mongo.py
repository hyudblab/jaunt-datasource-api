from . import mongo
from .traccar import UserDevice
from itertools import chain
import pandas as pd

db = mongo.db


def answered_by_dids(dids, db_tz="Etc/UTC", target_tz="Asia/Seoul", testers=False):
    uids = uids_from_dids(dids)
    df = answers_by_uids(uids, db_tz, target_tz)
    answered_pos = df["pos_id"].unique()
    return answered_pos.tolist()
    # return df[df["pos_id"].isin(answered_pos)]


def answer_by_pos_id(pos_id, target_tz):
    answers = db["answer"].aggregate(
        [
            {"$match": {"pos_id": pos_id}},
            {
                "$project": {
                    "_id": 0,
                    "pos_id": 1,
                    "qid": 1,
                    "choices": 1,
                    "datetime": {
                        "$dateToString": {
                            "format": "%Y-%m-%dT%H:%M:%S",
                            "date": {"$toDate": {"$toLong": "$timestamp"}},
                            "timezone": target_tz,
                        }
                    },
                }
            },
        ]
    )
    cols = ["pos_id", "qid", "choices", "datetime"]
    rows = []
    for row in list(answers):
        rows.append(list(row.values()))
    return cols, rows


def uids_from_dids(dids):
    uids = []
    for did in dids:
        uids.append(UserDevice.uid_from_did(did))
    print(uids)
    return uids


def df_stops(uids):
    stop_data = db["device"].find({"uniqueid": {"$in": uids}})
    all_stops = []
    for user in list(stop_data):
        all_stops.append(user["recentStops"])
    stop_df = pd.DataFrame(chain.from_iterable(all_stops))
    return stop_df


def answers_by_uids(uids, db_tz, target_tz):
    answer_data = db["answer"].aggregate([{"$match": {"uniqueid": {"$in": uids}}}])
    answer_df = mongo_to_df(answer_data)
    return answer_df


def mongo_to_df(mongo_data, db_tz="Etc/UTC", target_tz="Asia/Seoul"):
    df = pd.DataFrame(list(mongo_data))
    df["datetime"] = pd.to_datetime(df["timestamp"].astype("Q"), unit="ms")
    df.datetime = df.datetime.dt.tz_localize(db_tz).dt.tz_convert(target_tz)
    return df
