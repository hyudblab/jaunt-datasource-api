from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_pymongo import PyMongo

db = SQLAlchemy()
mongo = PyMongo()


def create_app():
    app = Flask(__name__, static_url_path="", static_folder="static/dist", template_folder="templates",)
    app.secret_key = "DBLAB_SECRET"
    app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0

    from .connect import open_db_props_as_url

    # Postgres
    pg_url = open_db_props_as_url("jaunt-deploy-traccar", "pg")
    app.config["SQLALCHEMY_DATABASE_URI"] = pg_url
    app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {"pool_pre_ping": True}
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    # Mongo
    mongo_url = open_db_props_as_url("jaunt-deploy-traccar-mongo", "mongo")
    app.config["MONGO_URI"] = mongo_url
    mongo.init_app(app)

    from .main import main as main_blueprint

    app.register_blueprint(main_blueprint)

    from .api import api as api_blueprint

    app.register_blueprint(api_blueprint, url_prefix="/api")

    from .file_down import file_down as file_down_blueprint

    app.register_blueprint(file_down_blueprint, url_prefix="/file")

    return app


# if __name__ == ("__main__"):


#     # Postgres
#     app.config['SQLALCHEMY_DATABASE_URI'] = open_db_props_as_url(
#         "jaunt-deploy-traccar", "pg")
#     db.init_app(app)

#     # Mongo
#     app.config["MONGO_URI"] = open_db_props_as_url(
#         "jaunt-deploy-traccar-mongo", "mongo")
#     mongo.init_app(app)

#     get_users(db)

#     app.run(debug=True, host="0.0.0.0")
