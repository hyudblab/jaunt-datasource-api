from .models import PreSurvey
from .traccar import UserDevice
import traceback

masking_keys = ["TCID", "phone"]


def query_survey_by_did(did):
    print(2)
    uid = UserDevice.uid_from_did(did)
    print(uid)
    try:
        survey = PreSurvey.query.filter(PreSurvey.TCID == uid).one()
        survey_dict = dict((col, getattr(survey, col)) for col in survey.__table__.columns.keys())
        for k in masking_keys:
            survey_dict.pop(k, None)
        return survey_dict
    except Exception:
        traceback.print_exc()
