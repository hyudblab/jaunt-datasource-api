import './index.scss'

const root = document.getElementById("root")
const users = document.getElementById("users")
const singleUser = document.getElementById("single-user")
const positions = document.getElementById("positions")

window.onload = async function () {

  createDateQuery(["54", "85"], "/positions", positions)
  createFetchLink("/api/users", users)
  createLink("/file/users", users)
  createUserDevice("85", singleUser)
  createSurvey("193255", document.getElementById("survey"))
  createUserSurveys("85", document.getElementById("surveys"))
  createUserPreSurvey("85", document.getElementById("pre-survey"))
}

function createUserPreSurvey(deviceid: number | string, targetDiv = root) {
  const url = `/user/${deviceid}/pre-surveys`
  createFetchLink("/api" + url, targetDiv)
  createLink("/file" + url, targetDiv)
}


function createUserSurveys(deviceid: number | string, targetDiv = root) {
  const url = `/user/${deviceid}/surveys`
  createFetchLink("/api" + url, targetDiv)
  createLink("/file" + url, targetDiv)
}

function createSurvey(pos_id: number | string, targetDiv = root) {
  const url = "/survey?pos_id=" + pos_id
  createFetchLink("/api" + url, targetDiv)
  createLink("/file" + url, targetDiv)
}

function createLink(href: string, targetDiv = root) {
  const wrapper = document.createElement("div")
  const anchor = document.createElement("a")
  anchor.href = href
  anchor.append(href)
  wrapper.appendChild(anchor)
  targetDiv.appendChild(wrapper)
  return anchor
}

function createFetchLink(href: string, targetDiv = root) {
  createLink(href, targetDiv).addEventListener("click", fetchJsonData)
}

function createUserDevice(uniqueid: string, targetDiv = root) {
  const url = "/user/" + uniqueid
  const apiUrl = "/api" + url
  const fileUrl = "/file" + url
  createFetchLink(apiUrl, targetDiv)
  createLink(fileUrl, targetDiv)
}

function createDateQuery(dids: string[], query: string, targetDiv = root) {
  let url = query
  const params = new URLSearchParams()
  dids.forEach(did => {
    params.append("did", did)
  })
  params.append("startdate", "2019-10-04T09:30:00Z")
  // params.append("enddate", "2019-11-04T10:30:00Z")
  params.append("tz", "Asia/Tokyo")
  url += "?" + params.toString()
  createFetchLink("/api" + url, targetDiv)
  createLink("/file" + url, targetDiv)
}

async function fetchJsonData(e: MouseEvent) {
  e.preventDefault()
  const href = this.href
  const data = await fetch(href).then(res => res.json())
  console.log(data)
}