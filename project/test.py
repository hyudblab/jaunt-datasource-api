from tests import point
from tests.upload_doc import upload_csv
import jsontree

# point.count()

upload_csv()


def read_survey_tree():
    with open("doc/survey_data.json", "r") as f:
        data = jsontree.loads(f.read())
        items = data.get("items")
        for item in items:
            print(item)
