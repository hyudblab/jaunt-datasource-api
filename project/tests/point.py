from .connect import open_db_props_as_url
from sqlalchemy import create_engine
from datetime import datetime, timedelta

engine = create_engine(open_db_props_as_url("traccar-scenario", "pg"))


def p():
    sql = """
        select td.name, tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul'
        from tc_devices td, tc_positions tp
        where td.id = tp.deviceid
        and tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' between %(t)s and %(t2)s        
        order by td.name
    """

    # one day

    t = datetime(2020, 6, 5, 13, 00)
    t2 = t + timedelta(days=1)
    params = {"t": str(t), "t2": str(t2)}
    run_query(sql, params, "One day")


def count():
    sql = """
        select td.name, count(tp.id)
        from tc_devices td, tc_positions tp
        where td.id = tp.deviceid
        and tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' between %(t)s and %(t2)s                
        group by td.name
        order by td.name
    """

    # one day
    starttime = datetime(2020, 6, 6, 7, 0)
    for hour in range(24):
        t = starttime + timedelta(hours=hour)
        t2 = t + timedelta(hours=1)
        params = {"t": str(t), "t2": str(t2)}
        run_query(sql, params, "Hour " + str(hour))


def run_query(sql, params, section="Query"):
    print(params)
    result = engine.execute(sql, params)
    print(section)
    for row in result:
        print(row)
