import pandas as pd
from sqlalchemy import create_engine
from .connect import open_db_props_as_url
from pprint import pprint


def upload_csv():
    df = pd.read_csv("doc/survey_data_full-utf8.csv")
    pprint(df.dtypes)

    # phone(int) to string: 010-
    df["phone"] = df["phone"].map(lambda x: "0" + str(x))

    tcids = []
    for row in df["TCID"]:
        tcid = str(row)
        if len(tcid) < 4:
            print(row)
            tcid = "0" + tcid
        tcids.append(tcid)

    print(tcids)

    df["TCID"] = tcids

    pprint(df.dtypes)

    pg_url = open_db_props_as_url("jaunt-deploy-traccar", "pg")
    engine = create_engine(pg_url)

    df.to_sql("pre_survey", con=engine, index=False, if_exists="replace")


if __name__ == "__main__":
    upload_csv()
