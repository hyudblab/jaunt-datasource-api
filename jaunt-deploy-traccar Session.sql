--@block
Select * from tc_devices where not(name like '@%%' or name like '#%%' or name = '01' or name = '02')

--@block
SELECT *
FROM tc_devices
WHERE tc_devices.name not similar to '%(test|#%%|@%%|01|02)%'


--@block
show timezone


--@block
SELECT tc_devices.id AS tc_devices_id, tc_devices.lastupdate AS tc_devices_lastupdate, tc_positions.id AS pos_id, tc_positions.devicetime AS tc_positions_devicetime, tc_positions.latitude AS tc_positions_latitude, tc_positions.longitude AS tc_positions_longitude, tc_positions.altitude AS tc_positions_altitude 
FROM tc_devices, tc_positions 
WHERE tc_devices.id IN (80, 85) AND tc_devices.id = tc_positions.deviceid 
AND ((tc_positions.devicetime at time zone 'Asia/Seoul' at time zone 'Asia/Seoul' BETWEEN '2019-11-04T09:30Z' AND '2019-11-04T10:00Z'