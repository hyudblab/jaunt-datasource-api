--@block
select td.name, tp.servertime  at time zone 'Etc/UTC' at time zone 'Asia/Seoul' as servertime,
  tp.devicetime at time zone 'Etc/UTC' at time zone 'Asia/Seoul' as devicetime
    from tc_devices td, tc_positions tp
    where td.id = tp.deviceid
    order by tp.servertime desc
    limit 50;

--@block
show timezone

--@block
select td.uniqueid, tp.* from tc_positions tp, tc_devices td
  where td.id = tp.deviceid
  and td.uniqueid in ('1032', '9628')
  limit 50

--@block
select * from tc_devices